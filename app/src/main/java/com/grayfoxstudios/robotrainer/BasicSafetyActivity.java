package com.grayfoxstudios.robotrainer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by boychane2 on 4/11/16.
 */
public class BasicSafetyActivity extends AppCompatActivity {

    private Tutorial basicSafetyTutorial;
    private Realm realm;
    private Button continueButton;
    private int numContinues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_saftey);

        final View rootView = findViewById(R.id.basic_safety_layout);

        realm = Realm.getDefaultInstance();
        basicSafetyTutorial = realm.where(Tutorial.class)
                .equalTo("title", HomeActivity.BASIC_SAFETY_TUTORIAL)
                .findFirst();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if(basicSafetyTutorial.getCurrentPage() != basicSafetyTutorial.getMaxProgress())
        {
            Fragment fragment = LessonFragment.newInstance(basicSafetyTutorial.getLessons().get(basicSafetyTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.basic_safety_content, fragment).commit();
        }
        else
        {
            realm.beginTransaction();
            basicSafetyTutorial.setCurrentPage(0);
            realm.commitTransaction();

            Fragment fragment = LessonFragment.newInstance(basicSafetyTutorial.getLessons().get(basicSafetyTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.basic_safety_content, fragment).addToBackStack(null).commit();
        }

        continueButton = (Button) findViewById(R.id.btn_basic_safety_continue);

        if(basicSafetyTutorial.getCurrentPage() == basicSafetyTutorial.getMaxProgress() - 1)
        {
            continueButton.setText("Complete");
        }
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkLessonComplete())
                {
                    if(basicSafetyTutorial.getCurrentPage() == basicSafetyTutorial.getMaxProgress())
                    {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("tutorial", basicSafetyTutorial.getTitle());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                    else
                    {
                        if(basicSafetyTutorial.getCurrentPage() + 1 <= basicSafetyTutorial.getMaxProgress() && basicSafetyTutorial.getCurrentPage() == basicSafetyTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            basicSafetyTutorial.setCurrentProgress(basicSafetyTutorial.getCurrentProgress() + 1);
                            basicSafetyTutorial.setCurrentPage(basicSafetyTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }
                        else if(basicSafetyTutorial.getCurrentPage() + 1 <= basicSafetyTutorial.getMaxProgress() && basicSafetyTutorial.getCurrentPage() < basicSafetyTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            basicSafetyTutorial.setCurrentPage(basicSafetyTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }

                        if(basicSafetyTutorial.getCurrentPage() < basicSafetyTutorial.getMaxProgress())
                        {
                            numContinues++;
                            Fragment newFragment = LessonFragment.newInstance(basicSafetyTutorial.getLessons().get(basicSafetyTutorial.getCurrentPage()));
                            fragmentManager.beginTransaction().replace(R.id.basic_safety_content, newFragment).addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();
                        }
                        else
                        {
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("tutorial", basicSafetyTutorial.getTitle());
                            setResult(RESULT_OK, returnIntent);
                            finish();
                        }

                        if(basicSafetyTutorial.getCurrentPage() == basicSafetyTutorial.getMaxProgress() - 1)
                        {
                            continueButton.setText("Complete");
                        }
                    }
                }
                else
                {
                    Snackbar.make(rootView, "The lesson has not been completed", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        Button closeButton = (Button) findViewById(R.id.btn_basic_safety_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", basicSafetyTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (basicSafetyTutorial.getCurrentPage() != 0)
        {
            if(numContinues > 0)
            {
                numContinues--;
                continueButton.setText("Continue");
                realm.beginTransaction();
                basicSafetyTutorial.setCurrentPage(basicSafetyTutorial.getCurrentPage() - 1);
                realm.commitTransaction();
                super.onBackPressed();
            }
            else
            {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", basicSafetyTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("tutorial", basicSafetyTutorial.getTitle());
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }


    private boolean checkLessonComplete() {

        RealmList<CheckBoxStatus> statuses = basicSafetyTutorial.getLessons().get(basicSafetyTutorial.getCurrentPage()).getCheckBoxStatuses();
        for(int i = 0; i < statuses.size(); i++)
        {
            if(!statuses.get(i).getStatus())
            {
                return false;
            }
        }
        return true;
    }
}