package com.grayfoxstudios.robotrainer;

/**
 * Created by boychane2 on 5/2/16.
 */
public class KeyTermsModel {

    String term;
    String definition;

    public KeyTermsModel(String term, String definition) {
        this.term = term;
        this.definition = definition;
    }

    public String getTerm() {
        return term;
    }

    public String getDefinition() {
        return definition;
    }
}
