package com.grayfoxstudios.robotrainer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by boychane2 on 4/11/16.
 */
public class RoverActivity extends AppCompatActivity {

    private Realm realm;
    private Tutorial roverTutorial;
    Button continueButton;
    private int numContinues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rover);

        final View rootView = findViewById(R.id.rover_layout);

        realm = Realm.getDefaultInstance();
        roverTutorial = realm.where(Tutorial.class)
                .equalTo("title", HomeActivity.ROVER_TUTORIAL)
                .findFirst();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if(roverTutorial.getCurrentPage() != roverTutorial.getMaxProgress())
        {
            Fragment fragment = LessonFragment.newInstance(roverTutorial.getLessons().get(roverTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.rover_content, fragment).commit();
        }
        else
        {
            realm.beginTransaction();
            roverTutorial.setCurrentPage(0);
            realm.commitTransaction();

            Fragment fragment = LessonFragment.newInstance(roverTutorial.getLessons().get(roverTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.rover_content, fragment).addToBackStack(null).commit();
        }

        continueButton = (Button) findViewById(R.id.btn_rover_continue);

        if(roverTutorial.getCurrentPage() == roverTutorial.getMaxProgress() - 1)
        {
            continueButton.setText("Complete");
        }
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkLessonComplete())
                {
                    if(roverTutorial.getCurrentPage() == roverTutorial.getMaxProgress())
                    {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("tutorial", roverTutorial.getTitle());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                    else
                    {
                        if(roverTutorial.getCurrentPage() + 1 <= roverTutorial.getMaxProgress() && roverTutorial.getCurrentPage() == roverTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            roverTutorial.setCurrentProgress(roverTutorial.getCurrentProgress() + 1);
                            roverTutorial.setCurrentPage(roverTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }
                        else if(roverTutorial.getCurrentPage() + 1 <= roverTutorial.getMaxProgress() && roverTutorial.getCurrentPage() < roverTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            roverTutorial.setCurrentPage(roverTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }

                        if(roverTutorial.getCurrentPage() < roverTutorial.getMaxProgress())
                        {
                            numContinues++;
                            Fragment newFragment = LessonFragment.newInstance(roverTutorial.getLessons().get(roverTutorial.getCurrentPage()));
                            fragmentManager.beginTransaction().replace(R.id.rover_content, newFragment).addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();
                        }
                        else
                        {
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("tutorial", roverTutorial.getTitle());
                            setResult(RESULT_OK, returnIntent);
                            finish();
                        }

                        if(roverTutorial.getCurrentPage() == roverTutorial.getMaxProgress() - 1)
                        {
                            continueButton.setText("Complete");
                        }
                    }
                }
                else
                {
                    Snackbar.make(rootView, "The lesson has not been completed", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        Button closeButton = (Button) findViewById(R.id.btn_rover_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", roverTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (roverTutorial.getCurrentPage() != 0)
        {
            if(numContinues > 0)
            {
                numContinues--;
                continueButton.setText("Continue");
                realm.beginTransaction();
                roverTutorial.setCurrentPage(roverTutorial.getCurrentPage() - 1);
                realm.commitTransaction();
                super.onBackPressed();
            }
            else
            {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", roverTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }

        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("tutorial", roverTutorial.getTitle());
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }


    private boolean checkLessonComplete() {

        RealmList<CheckBoxStatus> statuses = roverTutorial.getLessons().get(roverTutorial.getCurrentPage()).getCheckBoxStatuses();
        for(int i = 0; i < statuses.size(); i++)
        {
            if(!statuses.get(i).getStatus())
            {
                return false;
            }
        }
        return true;
    }
}
