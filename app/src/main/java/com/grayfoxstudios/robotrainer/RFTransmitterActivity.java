package com.grayfoxstudios.robotrainer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by boychane2 on 4/11/16.
 */
public class RFTransmitterActivity extends AppCompatActivity {

    private Realm realm;
    private Tutorial rfTransmitterTutorial;
    Button continueButton;
    private int numContinues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rf);

        final View rootView = findViewById(R.id.rf_layout);

        realm = Realm.getDefaultInstance();
        rfTransmitterTutorial = realm.where(Tutorial.class)
                                              .equalTo("title", HomeActivity.RF_TRANSMITTER_TUTORIAL)
                                              .findFirst();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if(rfTransmitterTutorial.getCurrentPage() != rfTransmitterTutorial.getMaxProgress())
        {
            Fragment fragment = LessonFragment.newInstance(rfTransmitterTutorial.getLessons().get(rfTransmitterTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.rf_content, fragment).commit();
        }
        else
        {
            realm.beginTransaction();
            rfTransmitterTutorial.setCurrentPage(0);
            realm.commitTransaction();

            Fragment fragment = LessonFragment.newInstance(rfTransmitterTutorial.getLessons().get(rfTransmitterTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.rf_content, fragment).addToBackStack(null).commit();
        }

        continueButton = (Button) findViewById(R.id.btn_rf_continue);

        if(rfTransmitterTutorial.getCurrentPage() == rfTransmitterTutorial.getMaxProgress() - 1)
        {
            continueButton.setText("Complete");
        }
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkLessonComplete())
                {
                    if(rfTransmitterTutorial.getCurrentPage() == rfTransmitterTutorial.getMaxProgress())
                    {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("tutorial", rfTransmitterTutorial.getTitle());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                    else
                    {
                        if(rfTransmitterTutorial.getCurrentPage() + 1 <= rfTransmitterTutorial.getMaxProgress() && rfTransmitterTutorial.getCurrentPage() == rfTransmitterTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            rfTransmitterTutorial.setCurrentProgress(rfTransmitterTutorial.getCurrentProgress() + 1);
                            rfTransmitterTutorial.setCurrentPage(rfTransmitterTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }
                        else if(rfTransmitterTutorial.getCurrentPage() + 1 <= rfTransmitterTutorial.getMaxProgress() && rfTransmitterTutorial.getCurrentPage() < rfTransmitterTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            rfTransmitterTutorial.setCurrentPage(rfTransmitterTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }

                        if(rfTransmitterTutorial.getCurrentPage() < rfTransmitterTutorial.getMaxProgress())
                        {
                            numContinues++;
                            Fragment newFragment = LessonFragment.newInstance(rfTransmitterTutorial.getLessons().get(rfTransmitterTutorial.getCurrentPage()));
                            fragmentManager.beginTransaction().replace(R.id.rf_content, newFragment).addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();
                        }
                        else
                        {
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("tutorial", rfTransmitterTutorial.getTitle());
                            setResult(RESULT_OK, returnIntent);
                            finish();
                        }

                        if(rfTransmitterTutorial.getCurrentPage() == rfTransmitterTutorial.getMaxProgress() - 1)
                        {
                            continueButton.setText("Complete");
                        }
                    }
                }
                else
                {
                    Snackbar.make(rootView, "The lesson has not been completed", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        Button closeButton = (Button) findViewById(R.id.btn_rf_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", rfTransmitterTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (rfTransmitterTutorial.getCurrentPage() != 0)
        {
            if(numContinues > 0)
            {
                numContinues--;
                continueButton.setText("Continue");
                realm.beginTransaction();
                rfTransmitterTutorial.setCurrentPage(rfTransmitterTutorial.getCurrentPage() - 1);
                realm.commitTransaction();
                super.onBackPressed();
            }
            else
            {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", rfTransmitterTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("tutorial", rfTransmitterTutorial.getTitle());
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }


    private boolean checkLessonComplete() {

        RealmList<CheckBoxStatus> statuses = rfTransmitterTutorial.getLessons().get(rfTransmitterTutorial.getCurrentPage()).getCheckBoxStatuses();
        for(int i = 0; i < statuses.size(); i++)
        {
            if(!statuses.get(i).getStatus())
            {
                return false;
            }
        }
        return true;
    }
}