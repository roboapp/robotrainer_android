package com.grayfoxstudios.robotrainer;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by boychane2 on 5/2/16.
 */
public class KeyTermsAdapter extends ArrayAdapter<KeyTermsModel> {
    Context context;
    int layoutResId;
    List<KeyTermsModel> data;

    public KeyTermsAdapter(Context context, int resource, List<KeyTermsModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layoutResId = resource;
        this.data = objects;
    }

    @Override
    public View getView(final int postition, View convertView, ViewGroup parent)
    {
        View row = convertView;
        final ContactHolder holder = new ContactHolder();


        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        row = inflater.inflate(layoutResId, parent, false);

        //holder = new ContactHolder();
        holder.term = (TextView)row.findViewById(R.id.tvTerm);
        holder.definition = (TextView)row.findViewById(R.id.tvDefinition);


        final KeyTermsModel contact = data.get(postition);
        holder.term.setText(contact.getTerm());
        holder.definition.setText(contact.getDefinition());


        return row;
    }

    @Override
    public int getCount()
    {
        return data.size();
    }
    static class ContactHolder
    {
        TextView term;
        TextView definition;

    }
}


