package com.grayfoxstudios.robotrainer;

import org.parceler.converter.CollectionParcelConverter;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Zach_macadams on 5/1/16.
 */
public abstract class RealmListParcelConverter<T extends RealmObject> extends CollectionParcelConverter<T, RealmList<T>> {

    @Override
    public RealmList<T> createCollection() {
        return new RealmList<T>();
    }
}
