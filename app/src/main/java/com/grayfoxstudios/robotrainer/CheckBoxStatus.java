package com.grayfoxstudios.robotrainer;

import org.parceler.Parcel;

import io.realm.CheckBoxStatusRealmProxy;
import io.realm.RealmObject;

/**
 * Created by Zach_macadams on 4/19/16.
 */

@Parcel(implementations = { CheckBoxStatusRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { CheckBoxStatus.class })
public class CheckBoxStatus extends RealmObject{

    private boolean status;

    public CheckBoxStatus() {}


    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
