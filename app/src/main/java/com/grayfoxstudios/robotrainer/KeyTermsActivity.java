package com.grayfoxstudios.robotrainer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by boychane2 on 4/11/16.
 */
public class KeyTermsActivity extends AppCompatActivity {
    private List<KeyTermsModel> keyTerms;
    private ListView keyList;
    private KeyTermsAdapter keyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_terms);
        keyTerms = new ArrayList<>();

        KeyTermsModel rover = new KeyTermsModel("Rover", "Exploration vehicle designed to move across a surface");
        KeyTermsModel rfTrans = new KeyTermsModel("RF-Transmitter", "Controller used to operate a rover");
        KeyTermsModel pivotTurn = new KeyTermsModel("Pivot Turn", "Both wheels rotate in opposite directions. Robot pivots over center point");
        KeyTermsModel motor = new KeyTermsModel("Motor", "Machine that produces motor of power for doing work");
        KeyTermsModel nest = new KeyTermsModel("Nest", "Collapsed state of mast");





        keyTerms.add(rover);
        keyTerms.add(rfTrans);
        keyTerms.add(pivotTurn);
        keyTerms.add(motor);
        keyTerms.add(nest);




        keyAdapter = new KeyTermsAdapter(this, R.layout.item_key_terms, keyTerms);

        keyList = (ListView)findViewById(R.id.lv_key_terms);

        keyList.setAdapter(keyAdapter);

        keyList.setItemsCanFocus(true);


    }
}
