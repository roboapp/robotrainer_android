package com.grayfoxstudios.robotrainer;

import android.os.Parcel;

import org.parceler.Parcels;

/**
 * Created by Zach_macadams on 5/1/16.
 */
public class CheckBoxStatusListParcelConverter extends RealmListParcelConverter<CheckBoxStatus> {
    @Override
    public void itemToParcel(CheckBoxStatus input, Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(input), 0);
    }

    @Override
    public CheckBoxStatus itemFromParcel(Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(CheckBoxStatus.class.getClassLoader()));
    }
}
