package com.grayfoxstudios.robotrainer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by boychane2 on 4/11/16.
 */
public class EmergencyActivity extends AppCompatActivity {

    private Realm realm;
    private Tutorial emergencyTutorial;
    Button continueButton;
    private int numContinues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        final View rootView = findViewById(R.id.emergency_layout);

        realm = Realm.getDefaultInstance();
        emergencyTutorial = realm.where(Tutorial.class)
                .equalTo("title", HomeActivity.EMERGENCY_TUTORIAL)
                .findFirst();

        final FragmentManager fragmentManager = getSupportFragmentManager();
        if(emergencyTutorial.getCurrentPage() != emergencyTutorial.getMaxProgress())
        {
            Fragment fragment = LessonFragment.newInstance(emergencyTutorial.getLessons().get(emergencyTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.emergency_content, fragment).commit();
        }
        else
        {
            realm.beginTransaction();
            emergencyTutorial.setCurrentPage(0);
            realm.commitTransaction();

            Fragment fragment = LessonFragment.newInstance(emergencyTutorial.getLessons().get(emergencyTutorial.getCurrentPage()));
            fragmentManager.beginTransaction().replace(R.id.emergency_content, fragment).addToBackStack(null).commit();
        }

        continueButton = (Button) findViewById(R.id.btn_emergency_continue);

        if(emergencyTutorial.getCurrentPage() == emergencyTutorial.getMaxProgress() - 1)
        {
            continueButton.setText("Complete");
        }
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkLessonComplete())
                {
                    if(emergencyTutorial.getCurrentPage() == emergencyTutorial.getMaxProgress())
                    {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("tutorial", emergencyTutorial.getTitle());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                    else
                    {
                        if(emergencyTutorial.getCurrentPage() + 1 <= emergencyTutorial.getMaxProgress() && emergencyTutorial.getCurrentPage() == emergencyTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            emergencyTutorial.setCurrentProgress(emergencyTutorial.getCurrentProgress() + 1);
                            emergencyTutorial.setCurrentPage(emergencyTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }
                        else if(emergencyTutorial.getCurrentPage() + 1 <= emergencyTutorial.getMaxProgress() && emergencyTutorial.getCurrentPage() < emergencyTutorial.getCurrentProgress())
                        {
                            realm.beginTransaction();
                            emergencyTutorial.setCurrentPage(emergencyTutorial.getCurrentPage() + 1);
                            realm.commitTransaction();
                        }

                        if(emergencyTutorial.getCurrentPage() < emergencyTutorial.getMaxProgress())
                        {
                            numContinues++;
                            Fragment newFragment = LessonFragment.newInstance(emergencyTutorial.getLessons().get(emergencyTutorial.getCurrentPage()));
                            fragmentManager.beginTransaction().replace(R.id.emergency_content, newFragment).addToBackStack(null).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).commit();
                        }
                        else
                        {
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("tutorial", emergencyTutorial.getTitle());
                            setResult(RESULT_OK, returnIntent);
                            finish();
                        }

                        if(emergencyTutorial.getCurrentPage() == emergencyTutorial.getMaxProgress() - 1)
                        {
                            continueButton.setText("Complete");
                        }
                    }
                }
                else
                {
                    Snackbar.make(rootView, "The lesson has not been completed", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        Button closeButton = (Button) findViewById(R.id.btn_emergency_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", emergencyTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (emergencyTutorial.getCurrentPage() != 0)
        {
            if(numContinues > 0)
            {
                numContinues--;
                continueButton.setText("Continue");
                realm.beginTransaction();
                emergencyTutorial.setCurrentPage(emergencyTutorial.getCurrentPage() - 1);
                realm.commitTransaction();
                super.onBackPressed();
            }
            else
            {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("tutorial", emergencyTutorial.getTitle());
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        }
        else
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("tutorial", emergencyTutorial.getTitle());
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }


    private boolean checkLessonComplete() {

        RealmList<CheckBoxStatus> statuses = emergencyTutorial.getLessons().get(emergencyTutorial.getCurrentPage()).getCheckBoxStatuses();
        for(int i = 0; i < statuses.size(); i++)
        {
            if(!statuses.get(i).getStatus())
            {
                return false;
            }
        }
        return true;
    }
}
