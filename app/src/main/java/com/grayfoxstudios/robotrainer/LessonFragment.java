package com.grayfoxstudios.robotrainer;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaControllerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.MediaController;
import android.widget.VideoView;

import org.parceler.Parcels;

import io.realm.Realm;

/**
 * Created by Zach_macadams on 4/19/16.
 */
public class LessonFragment extends Fragment {

    private Realm realm;

    public static LessonFragment newInstance(Lesson lesson) {

        Bundle args = new Bundle();

        args.putParcelable("lesson", Parcels.wrap(lesson));
        LessonFragment fragment = new LessonFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public LessonFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();
        realm = Realm.getDefaultInstance();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Bundle args = getArguments();

        final Lesson lesson = (Lesson) Parcels.unwrap(args.getParcelable("lesson"));

        View view =  inflater.inflate(lesson.getLayoutId(), container, false);

        if(lesson.getHasVideo())
        {
            VideoView video = (VideoView) view.findViewById(R.id.lesson_video);
            Uri uri = Uri.parse("android.resource://" + getActivity().getPackageName() +"/" + lesson.getVideoId());
            video.setVideoURI(uri);

            MediaController mediaController = new MediaController(getActivity());

            video.setMediaController(mediaController);
            mediaController.setAnchorView(video);
            video.start();
        }

        for(int i = 0; i < lesson.getCheckBoxStatuses().size(); i++)
        {
            final int pos = i;
            String checkBoxId = "chk_" + (i + 1);
            int resId = getActivity().getResources().getIdentifier(checkBoxId, "id", "com.grayfoxstudios.robotrainer");
            CheckBox checkBox = (CheckBox) view.findViewById(resId);
            checkBox.setChecked(lesson.getCheckBoxStatuses().get(i).getStatus());

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    Tutorial toEdit;
                    switch(lesson.getParentTutorial())
                    {
                        case HomeActivity.BASIC_SAFETY_TUTORIAL:
                            toEdit = realm.where(Tutorial.class)
                                    .equalTo("title", HomeActivity.BASIC_SAFETY_TUTORIAL)
                                    .findFirst();

                            realm.beginTransaction();
                            toEdit.getLessons().get(lesson.getLessonId() - 1).getCheckBoxStatuses().get(pos).setStatus(!lesson.getCheckBoxStatuses().get(pos).getStatus());
                            realm.commitTransaction();

                            break;

                        case HomeActivity.RF_TRANSMITTER_TUTORIAL:
                            toEdit = realm.where(Tutorial.class)
                                    .equalTo("title", HomeActivity.RF_TRANSMITTER_TUTORIAL)
                                    .findFirst();

                            realm.beginTransaction();
                            toEdit.getLessons().get(lesson.getLessonId() - 1).getCheckBoxStatuses().get(pos).setStatus(!lesson.getCheckBoxStatuses().get(pos).getStatus());
                            realm.commitTransaction();

                            break;

                        case HomeActivity.ROVER_TUTORIAL:
                            toEdit = realm.where(Tutorial.class)
                                    .equalTo("title", HomeActivity.ROVER_TUTORIAL)
                                    .findFirst();

                            realm.beginTransaction();
                            toEdit.getLessons().get(lesson.getLessonId() - 1).getCheckBoxStatuses().get(pos).setStatus(!lesson.getCheckBoxStatuses().get(pos).getStatus());
                            realm.commitTransaction();

                            break;

                        case HomeActivity.EMERGENCY_TUTORIAL:
                            toEdit = realm.where(Tutorial.class)
                                    .equalTo("title", HomeActivity.EMERGENCY_TUTORIAL)
                                    .findFirst();

                            realm.beginTransaction();
                            toEdit.getLessons().get(lesson.getLessonId() - 1).getCheckBoxStatuses().get(pos).setStatus(!lesson.getCheckBoxStatuses().get(pos).getStatus());
                            realm.commitTransaction();

                            break;
                    }
                }
            });
        }

        return view;
    }
}
