package com.grayfoxstudios.robotrainer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmList;


public class HomeActivity extends AppCompatActivity {

    private CircleProgressBar basicSafetyProgressBar, rfTransmitterProgressBar, roverProgressBar, emergencyProgressBar;
    private TextView basicSafetyProgressText, rfTransmitterProgressText, roverProgressText, emergencyProgressText;
    private ImageButton keyTermsImageButton, refresherImageButton;
    private Realm realm;

    public static final String BASIC_SAFETY_TUTORIAL = "Basic Safety";
    public static final String RF_TRANSMITTER_TUTORIAL = "RF Transmitter";
    public static final String ROVER_TUTORIAL = "RoverActivity";
    public static final String EMERGENCY_TUTORIAL = "EmergencyActivity Operation";

    private static final int BASIC_SAFETY_LESSON_1_CHECKBOX_COUNT = 8;
    private static final int BASIC_SAFETY_TUTORIAL_LESSON_COUNT = 1;

    private static final int RF_TRANSMITTER_LESSON_1_CHECKBOX_COUNT = 3;
    private static final int RF_TRANSMITTER_LESSON_2_CHECKBOX_COUNT = 3;
    private static final int RF_TRANSMITTER_LESSON_3_CHECKBOX_COUNT = 5;
    private static final int RF_TRANSMITTER_LESSON_4_CHECKBOX_COUNT = 5;
    private static final int RF_TRANSMITTER_LESSON_5_CHECKBOX_COUNT = 5;
    private static final int RF_TRANSMITTER_LESSON_6_CHECKBOX_COUNT = 5;
    private static final int RF_TRANSMITTER_TUTORIAL_LESSON_COUNT = 6;

    private static final int ROVER_LESSON_1_CHECKBOX_COUNT = 3;
    private static final int ROVER_LESSON_2_CHECKBOX_COUNT = 3;
    private static final int ROVER_LESSON_3_CHECKBOX_COUNT = 4;
    private static final int ROVER_LESSON_4_CHECKBOX_COUNT = 3;
    private static final int ROVER_TUTORIAL_LESSON_COUNT = 4;

    private static final int EMERGENCY_LESSON_1_CHECKBOX_COUNT = 8;
    private static final int EMERGENCY_TUTORIAL_LESSON_COUNT = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_drawer);

        realm = Realm.getDefaultInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black));

        if(realm.isEmpty())
        {
            initializeRealmDatabase();
        }

        Tutorial basicTutorial = realm.where(Tutorial.class)
                .equalTo("title", BASIC_SAFETY_TUTORIAL)
                .findFirst();

        Tutorial rfTutorial = realm.where(Tutorial.class)
                .equalTo("title", RF_TRANSMITTER_TUTORIAL)
                .findFirst();

        Tutorial roverTutorial = realm.where(Tutorial.class)
                .equalTo("title", ROVER_TUTORIAL)
                .findFirst();

        Tutorial emergencyTutorial = realm.where(Tutorial.class)
                .equalTo("title", EMERGENCY_TUTORIAL)
                .findFirst();

        basicSafetyProgressBar = (CircleProgressBar) findViewById(R.id.basicSafetyProgressBar);
        rfTransmitterProgressBar = (CircleProgressBar) findViewById(R.id.rfTransmitterProgressBar);
        roverProgressBar = (CircleProgressBar) findViewById(R.id.roverProgressBar);
        emergencyProgressBar = (CircleProgressBar) findViewById(R.id.emergencyProgressBar);

        basicSafetyProgressText = (TextView) findViewById(R.id.basicSafetyProgressText);
        rfTransmitterProgressText = (TextView) findViewById(R.id.rfTransmitterProgressText);
        roverProgressText = (TextView) findViewById(R.id.roverProgressText);
        emergencyProgressText = (TextView) findViewById(R.id.emergencyProgressText);

        basicSafetyProgressBar.setMax(basicTutorial.getMaxProgress());
        rfTransmitterProgressBar.setMax(rfTutorial.getMaxProgress());
        roverProgressBar.setMax(roverTutorial.getMaxProgress());
        emergencyProgressBar.setMax(emergencyTutorial.getMaxProgress());

        basicSafetyProgressBar.setProgress(basicTutorial.getCurrentProgress());
        rfTransmitterProgressBar.setProgress(rfTutorial.getCurrentProgress());
        roverProgressBar.setProgress(roverTutorial.getCurrentProgress());
        emergencyProgressBar.setProgress(emergencyTutorial.getCurrentProgress());

        String temp =  basicTutorial.getCurrentProgress() + "/" + basicTutorial.getMaxProgress();
        basicSafetyProgressText.setText(temp);

        temp =  rfTutorial.getCurrentProgress() + "/" + rfTutorial.getMaxProgress();
        rfTransmitterProgressText.setText(temp);

        temp =  roverTutorial.getCurrentProgress() + "/" + roverTutorial.getMaxProgress();
        roverProgressText.setText(temp);

        temp =  emergencyTutorial.getCurrentProgress() + "/" + emergencyTutorial.getMaxProgress();
        emergencyProgressText.setText(temp);


        basicSafetyProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeActivity.this, BasicSafetyActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        rfTransmitterProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeActivity.this, RFTransmitterActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        roverProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeActivity.this, RoverActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        emergencyProgressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HomeActivity.this, EmergencyActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        keyTermsImageButton = (ImageButton) findViewById(R.id.keyTermsButton);
        refresherImageButton = (ImageButton) findViewById(R.id.refresherButton);

        keyTermsImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, KeyTermsActivity.class);
                startActivity(intent);
            }
        });
        refresherImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RefresherActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == Activity.RESULT_OK)
        {
            String tutorialTitle = data.getStringExtra("tutorial");
            Tutorial returnedTutorial = realm.where(Tutorial.class)
                            .equalTo("title", tutorialTitle)
                            .findFirst();
            updateTutorialProgress(returnedTutorial);
        }
    }

    private void updateTutorialProgress(Tutorial tutorial) {

        CircleProgressBar barToUpdate;
        TextView textToUpdate;

        switch(tutorial.getTitle())
        {
            case BASIC_SAFETY_TUTORIAL:

                barToUpdate = basicSafetyProgressBar;
                textToUpdate = basicSafetyProgressText;
                break;

            case RF_TRANSMITTER_TUTORIAL:

                barToUpdate = rfTransmitterProgressBar;
                textToUpdate = rfTransmitterProgressText;
                break;

            case ROVER_TUTORIAL:

                barToUpdate = roverProgressBar;
                textToUpdate = roverProgressText;
                break;

            default:

                barToUpdate = emergencyProgressBar;
                textToUpdate = emergencyProgressText;
                break;
        }

        barToUpdate.setProgress(tutorial.getCurrentProgress());
        String temp =  tutorial.getCurrentProgress() + "/" + tutorial.getMaxProgress();
        textToUpdate.setText(temp);
    }


    private void initializeRealmDatabase() {

        // Initialize basicSafety tutorial
        Tutorial basicSafetyTutorial = new Tutorial();

        Lesson basicSafetyLesson = new Lesson();
        initializeLesson(basicSafetyLesson, 1, R.layout.fragment_basic_safety_lesson_one, BASIC_SAFETY_LESSON_1_CHECKBOX_COUNT, BASIC_SAFETY_TUTORIAL, false, 0);

        RealmList<Lesson> basicSafetyLessons = new RealmList<>();
        basicSafetyLessons.add(basicSafetyLesson);

        initializeTutorial(basicSafetyTutorial, BASIC_SAFETY_TUTORIAL, 0, 0, BASIC_SAFETY_TUTORIAL_LESSON_COUNT, basicSafetyLessons);

        realm.beginTransaction();
        realm.copyToRealm(basicSafetyTutorial);
        realm.commitTransaction();

        // Initialize rfTransmitter tutorial
        Tutorial rfTransmitterTutorial = new Tutorial();

        Lesson rfTransmitterLesson1 = new Lesson();
        initializeLesson(rfTransmitterLesson1, 1, R.layout.fragment_rf_lesson_one, RF_TRANSMITTER_LESSON_1_CHECKBOX_COUNT, RF_TRANSMITTER_TUTORIAL, true, R.raw.rf_on);

        Lesson rfTransmitterLesson2 = new Lesson();
        initializeLesson(rfTransmitterLesson2, 2, R.layout.fragment_rf_lesson_two, RF_TRANSMITTER_LESSON_2_CHECKBOX_COUNT, RF_TRANSMITTER_TUTORIAL, true, R.raw.set);

        Lesson rfTransmitterLesson3 = new Lesson();
        initializeLesson(rfTransmitterLesson3, 3, R.layout.fragment_rf_lesson_three, RF_TRANSMITTER_LESSON_3_CHECKBOX_COUNT, RF_TRANSMITTER_TUTORIAL, true, R.raw.movements);

        Lesson rfTransmitterLesson4 = new Lesson();
        initializeLesson(rfTransmitterLesson4, 4, R.layout.fragment_rf_lesson_four, RF_TRANSMITTER_LESSON_4_CHECKBOX_COUNT, RF_TRANSMITTER_TUTORIAL, true, R.raw.toggle_led_bar);

        Lesson rfTransmitterLesson5 = new Lesson();
        initializeLesson(rfTransmitterLesson5, 5, R.layout.fragment_rf_lesson_five, RF_TRANSMITTER_LESSON_5_CHECKBOX_COUNT, RF_TRANSMITTER_TUTORIAL, true, R.raw.toggle_shaft);

        Lesson rfTransmitterLesson6 = new Lesson();
        initializeLesson(rfTransmitterLesson6, 6, R.layout.fragment_rf_lesson_six, RF_TRANSMITTER_LESSON_6_CHECKBOX_COUNT, RF_TRANSMITTER_TUTORIAL, true, R.raw.toggle_beacon);

        RealmList<Lesson> rfTransmitterLessons = new RealmList<>();

        rfTransmitterLessons.add(rfTransmitterLesson1);
        rfTransmitterLessons.add(rfTransmitterLesson2);
        rfTransmitterLessons.add(rfTransmitterLesson3);
        rfTransmitterLessons.add(rfTransmitterLesson4);
        rfTransmitterLessons.add(rfTransmitterLesson5);
        rfTransmitterLessons.add(rfTransmitterLesson6);

        initializeTutorial(rfTransmitterTutorial, RF_TRANSMITTER_TUTORIAL, 0, 0, RF_TRANSMITTER_TUTORIAL_LESSON_COUNT, rfTransmitterLessons);

        realm.beginTransaction();
        realm.copyToRealm(rfTransmitterTutorial);
        realm.commitTransaction();


        // Initialize rover tutorial
        Tutorial roverTutorial = new Tutorial();

        Lesson roverLesson1 = new Lesson();
        initializeLesson(roverLesson1, 1, R.layout.fragment_rover_lesson_one, ROVER_LESSON_1_CHECKBOX_COUNT, ROVER_TUTORIAL, true, R.raw.turn_on);

        Lesson roverLesson2 = new Lesson();
        initializeLesson(roverLesson2, 2, R.layout.fragment_rover_lesson_two, ROVER_LESSON_2_CHECKBOX_COUNT, ROVER_TUTORIAL, true, R.raw.turn_off);

        Lesson roverLesson3 = new Lesson();
        initializeLesson(roverLesson3, 3, R.layout.fragment_rover_lesson_three, ROVER_LESSON_3_CHECKBOX_COUNT, ROVER_TUTORIAL, true, R.raw.plugin);

        Lesson roverLesson4 = new Lesson();
        initializeLesson(roverLesson4, 4, R.layout.fragment_rover_lesson_four, ROVER_LESSON_4_CHECKBOX_COUNT, ROVER_TUTORIAL, true, R.raw.unplug);

        RealmList<Lesson> roverLessons = new RealmList<>();

        roverLessons.add(roverLesson1);
        roverLessons.add(roverLesson2);
        roverLessons.add(roverLesson3);
        roverLessons.add(roverLesson4);

        initializeTutorial(roverTutorial, ROVER_TUTORIAL, 0, 0, ROVER_TUTORIAL_LESSON_COUNT, roverLessons);

        realm.beginTransaction();
        realm.copyToRealm(roverTutorial);
        realm.commitTransaction();


        // Initialize emergency tutorial
        Tutorial emergencyTutorial = new Tutorial();

        Lesson emergencyLesson1 = new Lesson();
        initializeLesson(emergencyLesson1, 1, R.layout.fragment_emergency_lesson_one, EMERGENCY_LESSON_1_CHECKBOX_COUNT, EMERGENCY_TUTORIAL, true, R.raw.killswitch);

        RealmList<Lesson> emergencyLessons = new RealmList<>();

        emergencyLessons.add(emergencyLesson1);

        initializeTutorial(emergencyTutorial, EMERGENCY_TUTORIAL, 0, 0, EMERGENCY_TUTORIAL_LESSON_COUNT, emergencyLessons);

        realm.beginTransaction();
        realm.copyToRealm(emergencyTutorial);
        realm.commitTransaction();
    }

    private void initializeLesson(Lesson lesson, int id, int layoutId, int checkBoxCount, String parentTutorial, boolean hasVideo, int videoId) {

        lesson.setLessonId(id);
        lesson.setLayoutId(layoutId);

        RealmList<CheckBoxStatus> checkBoxStatuses = new RealmList<>();
        for(int i = 0; i < checkBoxCount; i++)
        {
            CheckBoxStatus status = new CheckBoxStatus();
            status.setStatus(false);
            checkBoxStatuses.add(status);
        }
        lesson.setCheckBoxStatuses(checkBoxStatuses);
        lesson.setParentTutorial(parentTutorial);
        if(hasVideo)
        {
            lesson.setHasVideo(true);
            lesson.setVideoId(videoId);
        }
    }

    private void initializeTutorial(Tutorial tutorial, String title, int currentProgress, int currentPage, int maxProgress, RealmList<Lesson> lessons) {

        tutorial.setTitle(title);
        tutorial.setCurrentProgress(currentProgress);
        tutorial.setCurrentPage(currentPage);
        tutorial.setMaxProgress(maxProgress);
        tutorial.setLessons(lessons);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
        realm.close();
    }

}
