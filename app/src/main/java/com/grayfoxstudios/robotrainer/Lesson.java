package com.grayfoxstudios.robotrainer;


import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.io.Serializable;

import io.realm.LessonRealmProxy;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Zach_macadams on 3/28/16.
 */

@Parcel(implementations = { LessonRealmProxy.class },
        value = Parcel.Serialization.BEAN,
        analyze = { Lesson.class })
public class Lesson extends RealmObject {

    private int lessonId;
    private RealmList<CheckBoxStatus> checkBoxStatuses;
    private boolean hasVideo;
    private int layoutId;
    private String parentTutorial;
    private int videoId;


    public Lesson() {

    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public RealmList<CheckBoxStatus> getCheckBoxStatuses() {
        return checkBoxStatuses;
    }

    @ParcelPropertyConverter(CheckBoxStatusListParcelConverter.class)
    public void setCheckBoxStatuses(RealmList<CheckBoxStatus> checkBoxStatuses) {
        this.checkBoxStatuses = checkBoxStatuses;
    }

    public boolean getHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(boolean videoWatched) {
        this.hasVideo = videoWatched;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public String getParentTutorial() {
        return parentTutorial;
    }

    public void setParentTutorial(String parentTutorial) {
        this.parentTutorial = parentTutorial;
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }
}
